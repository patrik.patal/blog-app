<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\URL;

class Blog extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "blogs";
    protected $guarded = [];

    protected $appends = ['time_elapsed','posted_date'];

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function getImageAttribute($value){
        if($value == null)
            return URL::to('/'). '/blog.png';
        else
            return URL::to('/'). '/'. $value;

    }

    public function getTimeElapsedAttribute(){
        $now = strtotime(date('Y-m-d hh:mm:ss'));
        if($this->updated_at == null)
            return $this->humanTiming($now) . ' ago';
        else
            return $this->humanTiming(strtotime($this->updated_at)) . ' ago';

    }

    public function getPostedDateAttribute(){
        return date('Y-m-d', strtotime($this->created_at));
    }

    private function humanTiming ($time)
    {

        $time = time() - $time; // to get the time since that moment
        $time = ($time<1)? 1 : $time;
        $tokens = array (
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
        }

    }

}

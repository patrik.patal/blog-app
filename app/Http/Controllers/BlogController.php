<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BlogController extends Controller
{
    //
    public function getBlogs(Request $request){

        try{
            $blogs = Blog::query()
                ->where('user_id', $request->user()->id)
                ->orderBy('updated_at', 'desc')
                ->orderBy('created_at', 'desc')
                ->paginate(6);

            return $this->baseResponse(true,"Blog Lists", $blogs);
        }catch (\Exception $e){
            return $this->baseResponse(false,null,null, $e);
        }
    }

    public function getBlog(Request $request){

        try{
            $blog = Blog::query()
                ->where('id', $request->id)
                ->first();

            return $this->baseResponse(true,"Blog Details",$blog);
        }catch (\Exception $e){
            return $this->baseResponse(false,null,null, $e);
        }
    }

    public function saveBlog(Request $request){

        try{

            $validator = Validator::make($request->all(),[
                'title' => 'required',
                'description' => 'required'
            ]);

            if($validator->fails()){
                $error = $validator->getMessageBag();
                return $this->baseResponse(false,$error->first(),  $error);
            }

            if($request->hasFile('picture')){
                $file = $request->picture;
                $path = 'upload/'. 'user-'. $request->user()->id.'/';
                $fileName   = time() . $file->getClientOriginalName();
                Storage::disk('public')->put($path . $fileName, File::get($file));
                $filePath   = 'storage/'.$path .$fileName;
            }else
                $filePath = null;


            $blog = Blog::query()
                ->updateOrCreate(
                    ['id' => $request->id],
                    [
                        'title' => $request->title,
                        'description' => $request->description,
                        'image' => $filePath,
                        'user_id' => $request->user()->id
                    ]
                );

            return $this->baseResponse(true, "Saved blog item.", $blog);

        }catch (\Exception $e){

            return $this->baseResponse(false,null,$e);
        }
    }

    public function removeBlog(Request $request){
        try{
            $blog = Blog::query()
                ->where('id', $request->id)
                ->delete();

            return $this->baseResponse(true,"Deleted blog item.", $blog);
        }catch (\Exception $e){
            return $this->baseResponse(false,null,null,$e);
        }
    }
}

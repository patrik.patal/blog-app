import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// import { } from '@fortawesome/free-regular-svg-icons'

import {
  faUser, faLock, faSignOutAlt, faCog, faPlus, faArrowLeft, faEdit, faTrash, faSave, faTimesCircle , faTimes
} from '@fortawesome/free-solid-svg-icons'

import {
  faGithub
} from '@fortawesome/free-brands-svg-icons'

library.add(
  faUser, faLock, faSignOutAlt, faCog, faGithub, faPlus, faArrowLeft, faEdit, faTrash, faSave, faTimesCircle,faTimes
)

Vue.component('Fa', FontAwesomeIcon)
